<?php
/**
 * Writes new posts into wordpress programatically
 *
 * @package WordPress
 */

/** Make sure that the WordPress bootstrap has run before continuing. */
require(dirname(__FILE__) . '/wp-load.php');

global $user_ID; 

$post_if = $wpdb->get_var("SELECT count(post_title) FROM $wpdb->posts WHERE post_title  = 'Rogue One: A Star Wars Story (2016)' ");
if($post_if >= 1){ 
    //code here
    //
echo 'duplicate movie post found, skipping...'  ;
echo '  '  ;
return false;
}

$new_post = array(
'post_title' => 'Rogue One: A Star Wars Story (2016)',
'post_content' => 'Gareth Edwards - The Rebellion makes a risky move to steal the plans to the Death..',
'post_status' => 'publish',
'post_date' => date('Y-m-d H:i:s'),
'post_author' => $user_ID,
'post_type' => 'post',
'post_category' =>  array(1)
);
$post_id = wp_insert_post($new_post);

echo 'Successfully inserted movie'  ;
?>
