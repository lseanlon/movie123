 var express = require('express');
 var fs = require('fs');
 var request = require('request');
 var cheerio = require('cheerio');
 var app = express();


 var DownloadFromUrl = require('./Download');

 module.exports = function(_url) {

     request(_url, function(error, response, html) {
             if (error) {
                 console.log("Couldn’t get page because of error: " + error);
                 return;
             }

             var _sharedData = {};
             if (!error) {

                 var $ = cheerio.load(html);

                 //get the scirpt json of data
                 var jsonData = {};
                 $('script').each(function(i, elem) {
                     var markerPointInScript = "_sharedData";
                     var eachScriptInline = $(this).html();
                     if (eachScriptInline && eachScriptInline.indexOf(markerPointInScript) !== -1) {

                         jsonData = eachScriptInline;
                         jsonData = jsonData.replace('window.', '');
                         eval(jsonData);
                     }


                 });

                 var currentRow = {};
                 var media = _sharedData.entry_data.PostPage[0].media;

                 currentRow.username = media.owner.username;
                 currentRow.username = media.owner.username;
                 currentRow.fullname = media.owner.fullname;
                 currentRow.likes = media.likes.count;

                 currentRow.caption = media.caption;
                 currentRow.video_url = media.video_url;
                 currentRow.display_src = media.display_src;

                 var mediaUrlLink = (media.video_url || media.display_src);
                 if (mediaUrlLink) {
                     var filename = mediaUrlLink.substring(mediaUrlLink.lastIndexOf('/') + 1);
                     filename = filename.split("?")[0].split("#")[0];

                     currentRow.filename = filename;
                     DownloadFromUrl(mediaUrlLink, __dirname + "/../dist/media/" + filename, function() {
                         console.log('downloaded media ' + mediaUrlLink);
                     });

                 }

                 /* 
 	 		var rowLine="\n"+'[newrow]==>'  ;
	 		var media = _sharedData.entry_data.PostPage[0].media ; 

 	 		  ownerLine = "\n"+ '[username]==>'   + media.owner.username ;
	 		 rowLine+=ownerLine;

 	 		  ownerLine = "\n"+ '[fullname]==>'   + media.owner.fullname ;
	 		 rowLine+=ownerLine;

	 		  ownerLine = "\n"+ '[likes]==>'   +  media.likes.count  ;
	 		 rowLine+=ownerLine;
  
	 		  ownerLine = "\n"+ '[caption]==>'   +  media.caption   ;
	 		 rowLine+=ownerLine;
 
 		  	ownerLine = "\n"+ '[video_url]==>'   +  media.video_url   ;
	 		 rowLine+=ownerLine;
  
 		  	ownerLine = "\n"+ '[display_src]==>'   +  media.display_src   ;
	 		 rowLine+=ownerLine;

   
 			var mediaUrlLink = (media.video_url    || media.display_src  );
 			if(mediaUrlLink){
				var filename = mediaUrlLink.substring(mediaUrlLink.lastIndexOf('/')+1);
				filename =filename.split("?")[0].split("#")[0];
		 		var captionLine ="\n" +'[filename]==>'+  filename ; 
	 			rowLine+=captionLine;  

				DownloadFromUrl(mediaUrlLink,  __dirname  + "/../dist/media/"+ filename, function(){
				  console.log('downloaded media ' + mediaUrlLink);
				});	

 			}

 			console.log(rowLine);	
 			var timestamp = new Date().getTime().toString();
 			// var filePathName =  __dirname  + "/../dist/data-"+ timestamp+".txt";
*/

 			 var filePathName =  __dirname  + "/../dist/data.txt";

            fs.appendFile(filePathName, JSON.stringify(currentRow) +",", function(err) {
                if (err) {
                    return console.log(err);
                }
                //sample result
                //{},{},
                //required additional
                //[ ...{}]
                console.log("The file was saved!");
            });
 
         }
         })
         return _url; 
 };
