var cheerio = require('cheerio');
var fs = require('fs');

var download = require('download');

var jsonPathPage = fs.readFileSync('./dist/data-category-movies-details.txt', 'utf8');
var movieList = JSON.parse(jsonPathPage);

var listOfMovies = [];
movieList.forEach(function(_item) {

    //open all movie , read all localfullpath html, extract out all data
    //save into output

 
    var title = _item.title;
    var link = _item.link;
    var category = _item.category ? _item.category : './dist/categoryUndefined/'; 
    var localfullpath =  _item.localfullpath; 

    var htmlpagemovie = fs.readFileSync(localfullpath, 'utf8');
    var $ = cheerio.load(htmlpagemovie);
            var rating = $('#movie-mark').html();
            var desc =$('.desc').html();
            var vidlink =$('video').prop('src') ; 
            var director  = $('strong:contains("Director")').parent().find('a').html();
            _item.rating = rating;
            _item.desc = desc;
            _item.vidlink = vidlink; 
            _item.director = director;   
  
     listOfMovies.push(_item);
   

});

 
var savePath = "./dist/data-category-movies-details-watch.txt";
fs.appendFile(savePath, JSON.stringify(listOfMovies), function(err) {
    if (err) {
        return console.log(err);
    }

    console.log("The file was saved!", savePath);
});
