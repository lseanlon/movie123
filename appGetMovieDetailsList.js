var cheerio = require('cheerio');
var fs = require('fs');

var download = require('download');

for (var i = 0; i < 100; i++) {

    try {



        var jsonPathPage = fs.readFileSync('./dist/data-category-movies.txt' + "." + (i + 1), 'utf8');
        var movieList = JSON.parse(jsonPathPage);

        var listOfMovies = [];
        movieList.forEach(function(_item) {

            //open all movie based on link , key, download into movieCategoryName


            var title = _item.title;
            var link = _item.link;
            var category = _item.category ? _item.category : './dist/categoryUndefined/';
            var sanitizemoviename = link.substring(link.indexOf('film'), link.length - 2);
            var localpath = category + "/film";

            var localfullpath = localpath + "/" + link.replace('http://123movies.is/film/', '');
            localfullpath = localfullpath.replace('//', '/');
            var row = {
                'title': title,
                'link': link,
                'category': category,
                'localpath': localpath,
                'localfullpath': localfullpath
            }


            listOfMovies.push(row);
            console.log('start download - ', link);
            download(link + "watching.html", localpath).then(() => {
                console.log('done download - ', link);
            });



        });


        var savePath = "./dist/data-category-movies-details.txt";
        fs.writeFileSync(savePath, JSON.stringify(listOfMovies));

    } catch (e) {
        console.log("err", e);
    }
}
