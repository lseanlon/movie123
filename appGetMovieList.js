var cheerio = require('cheerio');
var fs = require('fs');

var download = require('download');
var categoryListString = fs.readFileSync('./dist/data-categorylist.txt', 'utf8');
var categoryList = JSON.parse(categoryListString);

var jsonPathPage = fs.readFileSync('./dist/data-category-page.txt', 'utf8');
var categoryPageList = JSON.parse(jsonPathPage);

var listOfMovies=[];
var listOfCategoryMovies=[];
categoryPageList.forEach(function(_item) {

    //open each category
    //read all index html

    //determine folder, determine max pages to read
    var localpath = _item.path;
    var maxpage = _item.lastIndexPage;

    //read each page and take out all the movie list and drop into file
    //data-category-movie

    for (var idx = 1; idx <= maxpage; idx++) {
        var indexUrl =localpath + "" + idx;
         // console.log(indexUrl);
 

        try {
         
          var htmlcategory = fs.readFileSync(indexUrl, 'utf8');
            var $ = cheerio.load(htmlcategory);
                  if ($('[data-movie-id]')) {
                    var currow={},title,link,image,id;

                      $('[data-movie-id]').each( function( index, elem ) {
                         

                            var currow={
                                'title':$(elem).find('.mli-info h2').html(),
                                'link':$(elem).find('a').prop('href'), 
                                'category': localpath, 
                                'image':$(elem).find('a img').prop('src'),
                                'id':$(elem).attr('data-movie-id'), 
                            } ;
                          listOfCategoryMovies.push(currow) ;
                     });

                 // console.log(  listOfCategoryMovies);
                }


        } catch (e) {
            // console.log('Error:', e);
        } 

    }
 
 
   console.log(  listOfCategoryMovies.length);
   var savePath= "./dist/data-category-movies.txt";
    var chunkSize=100;
    for (var i = 0; i < listOfCategoryMovies.length; i += chunkSize) {
        var curChunk= (listOfCategoryMovies.slice(i, i + chunkSize)); 
         fs.writeFileSync(savePath +"." + (i+1), JSON.stringify( curChunk)   );
    }
 


   //divide 100 record - 1 file
    // var savePath= "./dist/data-category-movies.txt";
    //     fs.appendFile(savePath, JSON.stringify( listOfCategoryMovies)  , function(err) {
    //                 if (err) {
    //                     return console.log(err);
    //                 }

    //                 console.log("The file was saved!",savePath);
    //     });




});
